## Modules

- [csv-service](#module_csv-service) : <code>service</code>
  - [.settings](#module_csv-service.settings)
    - [.diffDay](#module_csv-service.settings.diffDay) : <code>number</code>
    - [.expireTime](#module_csv-service.settings.expireTime) : <code>number</code>
  - [.mixins](#module_csv-service.mixins)
  - [.actions](#module_csv-service.actions)
    - [.saveInCache](#module_csv-service.actions.saveInCache) ⇒ <code>boolean</code>
    - [.getCSVName](#module_csv-service.actions.getCSVName) ⇒ <code>string</code>
    - [.getAllCSVFromCache](#module_csv-service.actions.getAllCSVFromCache) ⇒ <code>Buffer</code>
    - [.getSpecificCSVFromCache](#module_csv-service.actions.getSpecificCSVFromCache) ⇒ <code>buffer</code>
  - [.methods](#module_csv-service.methods)
    - [.normaliseDate(date)](#module_csv-service.methods.normaliseDate) ⇒ <code>string</code>
    - [.parseKey(key)](#module_csv-service.methods.parseKey) ⇒ <code>string</code>
    - [.parseDate(data)](#module_csv-service.methods.parseDate) ⇒ <code>object</code>
    - [.nameCSVFile(diffDay)](#module_csv-service.methods.nameCSVFile) ⇒ <code>string</code>
    - [.checkErrosInCacheResponse(response)](#module_csv-service.methods.checkErrosInCacheResponse) ⇒ <code>boolean</code>
    - [.getData(keys)](#module_csv-service.methods.getData) ⇒ <code>buffer</code>
    - [.getAllFilenamesFromCache()](#module_csv-service.methods.getAllFilenamesFromCache) ⇒ <code>array</code>
    - [.save(filename, data, expireTime)](#module_csv-service.methods.save) ⇒ <code>boolean</code>
- [email-service](#module_email-service) : <code>service</code>
  - [.settings](#module_email-service.settings)
  - [.mixins](#module_email-service.mixins)
  - [.actions](#module_email-service.actions)
    - [.sendEmail](#module_email-service.actions.sendEmail) ⇒ <code>object</code>
    - [.sendError](#module_email-service.actions.sendError) ⇒ <code>object</code>
    - [.emailList](#module_email-service.actions.emailList) ⇒ <code>array</code>
    - [.addEmail](#module_email-service.actions.addEmail) ⇒ <code>object</code>
    - [.delEmail](#module_email-service.actions.delEmail) ⇒ <code>object</code>
  - [.methods](#module_email-service.methods)
    - [.getContacts()](#module_email-service.methods.getContacts) ⇒ <code>array</code>
    - [.parseSaveCacheResponse(array, emails)](#module_email-service.methods.parseSaveCacheResponse) ⇒ <code>array</code>
    - [.manageContacts(type, emails)](#module_email-service.methods.manageContacts) ⇒ <code>array</code>
  - [.started()](#module_email-service.started)
- [painelweb-service](#module_painelweb-service) : <code>service</code>
  - [.settings](#module_painelweb-service.settings)
  - [.mixins](#module_painelweb-service.mixins)
  - [.actions](#module_painelweb-service.actions)
    - [.upload](#module_painelweb-service.actions.upload) ⇒ <code>object</code>
  - [.methods](#module_painelweb-service.methods)
    - [.createJSON()](#module_painelweb-service.methods.createJSON) ⇒ <code>buffer</code>
    - [.dataParse(files, filePath)](#module_painelweb-service.methods.dataParse) ⇒ <code>object</code>
- [workflow-service](#module_workflow-service) : <code>service</code>
  - [.settings](#module_workflow-service.settings)
  - [.dependencies](#module_workflow-service.dependencies)
  - [.mixins](#module_workflow-service.mixins)
  - [.actions](#module_workflow-service.actions)
    - [.data](#module_workflow-service.actions.data) ⇒ <code>String</code>
  - [.events](#module_workflow-service.events)
    - [.queue.CSV](#module_workflow-service.events.queue.CSV)
    - [.queue.succeeded](#module_workflow-service.events.queue.succeeded)
    - [.queue.failed](#module_workflow-service.events.queue.failed)
  - [.methods](#module_workflow-service.methods)
    - [.timer()](#module_workflow-service.methods.timer) ⇒ <code>object</code>
    - [.save(data)](#module_workflow-service.methods.save) ⇒ <code>object</code>
    - [.push()](#module_workflow-service.methods.push) ⇒ <code>string</code>
  - [.started()](#module_workflow-service.started)

## Mixins

- [axios](#axios)
  - [.settings](#axios.settings)
    - [.axios](#axios.settings.axios) : <code>Object</code>
      - [.retry](#axios.settings.axios.retry) : <code>Number</code>
      - [.delay](#axios.settings.axios.delay) : <code>Number</code>
      - [.options](#axios.settings.axios.options) : <code>Object</code>
  - [.methods](#axios.methods)
    - [.axios(retry, delay, options)](#axios.methods.axios) ⇒ <code>object</code> \| <code>function</code>
  - [.created()](#axios.created)
- [email](#email)
  - [.settings](#email.settings)
    - [.email](#email.settings.email) : <code>Object</code>
      - [.key](#email.settings.email.key) : <code>String</code>
  - [.methods](#email.methods)
    - [.Message](#email.methods.Message)
      - [.template](#email.methods.Message.template) : <code>function</code>
      - [.addAttachment](#email.methods.Message.addAttachment) : <code>function</code>
      - [.addHTML](#email.methods.Message.addHTML) : <code>function</code>
      - [.get](#email.methods.Message.get) ⇒ <code>object</code>
    - [.messageBuild(to, from, subject, text, html, attachments)](#email.methods.messageBuild) ⇒ <code>message</code>
    - [.sendEmail(message)](#email.methods.sendEmail) ⇒ <code>object</code>
  - [.started()](#email.started)
- [queue](#queue)
  - [.methods](#queue.methods)
    - [.createJob(jobName, options)](#queue.methods.createJob) ⇒ <code>object</code>
- [redis](#redis)
  - [.settings](#redis.settings)
    - [.redis](#redis.settings.redis) : <code>Object</code>
  - [.methods](#redis.methods)
    - [.connect()](#redis.methods.connect) ⇒ <code>string</code>
    - [.checkConnection()](#redis.methods.checkConnection) ⇒ <code>string</code>
    - [.disconnect()](#redis.methods.disconnect) ⇒ <code>string</code>
  - [.created()](#redis.created)
  - [.started()](#redis.started)
  - [.stopped()](#redis.stopped)

<a name="module_csv-service"></a>

## csv-service : <code>service</code>

Service to save/get csv file from redis;

**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>mixins</td><td><code>object</code></td><td><p>mixins</p>
</td>
    </tr><tr>
    <td>dependencies</td><td><code>object</code></td><td><p>dependencies</p>
</td>
    </tr><tr>
    <td>started</td><td><code>function</code></td><td><p>Service started lifecycle event handler</p>
</td>
    </tr>  </tbody>
</table>

- [csv-service](#module_csv-service) : <code>service</code>
  - [.settings](#module_csv-service.settings)
    - [.diffDay](#module_csv-service.settings.diffDay) : <code>number</code>
    - [.expireTime](#module_csv-service.settings.expireTime) : <code>number</code>
  - [.mixins](#module_csv-service.mixins)
  - [.actions](#module_csv-service.actions)
    - [.saveInCache](#module_csv-service.actions.saveInCache) ⇒ <code>boolean</code>
    - [.getCSVName](#module_csv-service.actions.getCSVName) ⇒ <code>string</code>
    - [.getAllCSVFromCache](#module_csv-service.actions.getAllCSVFromCache) ⇒ <code>Buffer</code>
    - [.getSpecificCSVFromCache](#module_csv-service.actions.getSpecificCSVFromCache) ⇒ <code>buffer</code>
  - [.methods](#module_csv-service.methods)
    - [.normaliseDate(date)](#module_csv-service.methods.normaliseDate) ⇒ <code>string</code>
    - [.parseKey(key)](#module_csv-service.methods.parseKey) ⇒ <code>string</code>
    - [.parseDate(data)](#module_csv-service.methods.parseDate) ⇒ <code>object</code>
    - [.nameCSVFile(diffDay)](#module_csv-service.methods.nameCSVFile) ⇒ <code>string</code>
    - [.checkErrosInCacheResponse(response)](#module_csv-service.methods.checkErrosInCacheResponse) ⇒ <code>boolean</code>
    - [.getData(keys)](#module_csv-service.methods.getData) ⇒ <code>buffer</code>
    - [.getAllFilenamesFromCache()](#module_csv-service.methods.getAllFilenamesFromCache) ⇒ <code>array</code>
    - [.save(filename, data, expireTime)](#module_csv-service.methods.save) ⇒ <code>boolean</code>

<a name="module_csv-service.settings"></a>

### csv-service.settings

Settings

**Kind**: static property of [<code>csv-service</code>](#module_csv-service)

- [.settings](#module_csv-service.settings)
  - [.diffDay](#module_csv-service.settings.diffDay) : <code>number</code>
  - [.expireTime](#module_csv-service.settings.expireTime) : <code>number</code>

<a name="module_csv-service.settings.diffDay"></a>

#### settings.diffDay : <code>number</code>

date difference for business rule for generating csv file name

**Kind**: static property of [<code>settings</code>](#module_csv-service.settings)  
<a name="module_csv-service.settings.expireTime"></a>

#### settings.expireTime : <code>number</code>

cache expiration time

**Kind**: static property of [<code>settings</code>](#module_csv-service.settings)  
<a name="module_csv-service.mixins"></a>

### csv-service.mixins

Mixins

**Kind**: static property of [<code>csv-service</code>](#module_csv-service)  
**Mixes**: [<code>redis</code>](#redis)  
<a name="module_csv-service.actions"></a>

### csv-service.actions

Actions

**Kind**: static property of [<code>csv-service</code>](#module_csv-service)

- [.actions](#module_csv-service.actions)
  - [.saveInCache](#module_csv-service.actions.saveInCache) ⇒ <code>boolean</code>
  - [.getCSVName](#module_csv-service.actions.getCSVName) ⇒ <code>string</code>
  - [.getAllCSVFromCache](#module_csv-service.actions.getAllCSVFromCache) ⇒ <code>Buffer</code>
  - [.getSpecificCSVFromCache](#module_csv-service.actions.getSpecificCSVFromCache) ⇒ <code>buffer</code>

<a name="module_csv-service.actions.saveInCache"></a>

#### actions.saveInCache ⇒ <code>boolean</code>

save CSV in cache

**Kind**: static property of [<code>actions</code>](#module_csv-service.actions)  
**Returns**: <code>boolean</code> - - return true if success  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>object</code></td><td><p>data object to convert into csv</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.actions.getCSVName"></a>

#### actions.getCSVName ⇒ <code>string</code>

return and delete csv filename

**Kind**: static property of [<code>actions</code>](#module_csv-service.actions)  
**Returns**: <code>string</code> - - csv filename  
**Actions**:  
<a name="module_csv-service.actions.getAllCSVFromCache"></a>

#### actions.getAllCSVFromCache ⇒ <code>Buffer</code>

get all csv data from cache

**Kind**: static property of [<code>actions</code>](#module_csv-service.actions)  
**Returns**: <code>Buffer</code> - - Buffer  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>date</td><td><code>date</code></td><td><p>date to fetch data on cache</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.actions.getSpecificCSVFromCache"></a>

#### actions.getSpecificCSVFromCache ⇒ <code>buffer</code>

get specific csv file from cache

**Kind**: static property of [<code>actions</code>](#module_csv-service.actions)  
**Returns**: <code>buffer</code> - - csv buffer  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>filename</td><td><code>string</code></td><td><p>name of csv file to retrieve in cache</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods"></a>

### csv-service.methods

Methods

**Kind**: static property of [<code>csv-service</code>](#module_csv-service)

- [.methods](#module_csv-service.methods)
  - [.normaliseDate(date)](#module_csv-service.methods.normaliseDate) ⇒ <code>string</code>
  - [.parseKey(key)](#module_csv-service.methods.parseKey) ⇒ <code>string</code>
  - [.parseDate(data)](#module_csv-service.methods.parseDate) ⇒ <code>object</code>
  - [.nameCSVFile(diffDay)](#module_csv-service.methods.nameCSVFile) ⇒ <code>string</code>
  - [.checkErrosInCacheResponse(response)](#module_csv-service.methods.checkErrosInCacheResponse) ⇒ <code>boolean</code>
  - [.getData(keys)](#module_csv-service.methods.getData) ⇒ <code>buffer</code>
  - [.getAllFilenamesFromCache()](#module_csv-service.methods.getAllFilenamesFromCache) ⇒ <code>array</code>
  - [.save(filename, data, expireTime)](#module_csv-service.methods.save) ⇒ <code>boolean</code>

<a name="module_csv-service.methods.normaliseDate"></a>

#### methods.normaliseDate(date) ⇒ <code>string</code>

transform date in string YYYY-MM-DD

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>string</code> - - YYYY-MM-DD  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>date</td><td><code>date</code></td><td><p>date</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods.parseKey"></a>

#### methods.parseKey(key) ⇒ <code>string</code>

remove prefix from key

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>string</code> - - keyName  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>key</td><td><code>string</code></td><td><p>prefix:keyName</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods.parseDate"></a>

#### methods.parseDate(data) ⇒ <code>object</code>

parse data to object with headers and line to build csv

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>object</code> - - { headers, line }  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>object</code></td><td><p>data object</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods.nameCSVFile"></a>

#### methods.nameCSVFile(diffDay) ⇒ <code>string</code>

create a CSV name
Busyness rule: File name = {{timestamp}}-{{actual date - diffDay}}.csv

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>string</code> - csv name  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>diffDay</td><td><code>number</code></td><td><p>number of days difference from the current date to generate the file name</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods.checkErrosInCacheResponse"></a>

#### methods.checkErrosInCacheResponse(response) ⇒ <code>boolean</code>

check if all steps to save cache is success

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>boolean</code> - - true if all things ok  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>response</td><td><code>array</code></td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods.getData"></a>

#### methods.getData(keys) ⇒ <code>buffer</code>

get all keys from redis and parse to buffer

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>buffer</code> - - buffer of all data from array keys  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>keys</td><td><code>array</code> | <code>string</code></td><td><p>[prefix:keyName]</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_csv-service.methods.getAllFilenamesFromCache"></a>

#### methods.getAllFilenamesFromCache() ⇒ <code>array</code>

get all keys from cache

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>array</code> - - [keyName]  
**Methods**:  
<a name="module_csv-service.methods.save"></a>

#### methods.save(filename, data, expireTime) ⇒ <code>boolean</code>

save CSV in cache

**Kind**: static method of [<code>methods</code>](#module_csv-service.methods)  
**Returns**: <code>boolean</code> - - return true if success  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>filename</td><td><code>string</code></td><td><p>name of file to save a key in redis</p>
</td>
    </tr><tr>
    <td>data</td><td><code>object</code></td><td><p>header and line of csv file</p>
</td>
    </tr><tr>
    <td>expireTime</td><td><code>number</code></td><td><p>expire time.</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service"></a>

## email-service : <code>service</code>

Service mixin to create and send email

**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>mixins</td><td><code>object</code></td><td><p>mixins</p>
</td>
    </tr><tr>
    <td>dependencies</td><td><code>object</code></td><td><p>dependencies</p>
</td>
    </tr><tr>
    <td>started</td><td><code>function</code></td><td><p>Service started lifecycle event handler</p>
</td>
    </tr>  </tbody>
</table>

- [email-service](#module_email-service) : <code>service</code>
  - [.settings](#module_email-service.settings)
  - [.mixins](#module_email-service.mixins)
  - [.actions](#module_email-service.actions)
    - [.sendEmail](#module_email-service.actions.sendEmail) ⇒ <code>object</code>
    - [.sendError](#module_email-service.actions.sendError) ⇒ <code>object</code>
    - [.emailList](#module_email-service.actions.emailList) ⇒ <code>array</code>
    - [.addEmail](#module_email-service.actions.addEmail) ⇒ <code>object</code>
    - [.delEmail](#module_email-service.actions.delEmail) ⇒ <code>object</code>
  - [.methods](#module_email-service.methods)
    - [.getContacts()](#module_email-service.methods.getContacts) ⇒ <code>array</code>
    - [.parseSaveCacheResponse(array, emails)](#module_email-service.methods.parseSaveCacheResponse) ⇒ <code>array</code>
    - [.manageContacts(type, emails)](#module_email-service.methods.manageContacts) ⇒ <code>array</code>
  - [.started()](#module_email-service.started)

<a name="module_email-service.settings"></a>

### email-service.settings

Settings

**Kind**: static property of [<code>email-service</code>](#module_email-service)  
<a name="module_email-service.mixins"></a>

### email-service.mixins

mixins

**Kind**: static property of [<code>email-service</code>](#module_email-service)  
**Mixes**: [<code>email</code>](#email), [<code>redis</code>](#redis)  
<a name="module_email-service.actions"></a>

### email-service.actions

Actions

**Kind**: static property of [<code>email-service</code>](#module_email-service)

- [.actions](#module_email-service.actions)
  - [.sendEmail](#module_email-service.actions.sendEmail) ⇒ <code>object</code>
  - [.sendError](#module_email-service.actions.sendError) ⇒ <code>object</code>
  - [.emailList](#module_email-service.actions.emailList) ⇒ <code>array</code>
  - [.addEmail](#module_email-service.actions.addEmail) ⇒ <code>object</code>
  - [.delEmail](#module_email-service.actions.delEmail) ⇒ <code>object</code>

<a name="module_email-service.actions.sendEmail"></a>

#### actions.sendEmail ⇒ <code>object</code>

send an email stating that the process has ended

**Kind**: static property of [<code>actions</code>](#module_email-service.actions)  
**Returns**: <code>object</code> - - return of email requisition  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>attachments</td><td><code>array</code></td><td><p>array of attachments</p>
</td>
    </tr><tr>
    <td>text</td><td><code>string</code></td><td><p>body content</p>
</td>
    </tr><tr>
    <td>subject</td><td><code>string</code></td><td><p>subject content</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service.actions.sendError"></a>

#### actions.sendError ⇒ <code>object</code>

send an email stating that the process has ended with error

**Kind**: static property of [<code>actions</code>](#module_email-service.actions)  
**Returns**: <code>object</code> - - return of email requisition  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>error</td><td><code>string</code></td><td><p>error messageBuild</p>
</td>
    </tr><tr>
    <td>text</td><td><code>string</code></td><td><p>body content</p>
</td>
    </tr><tr>
    <td>subject</td><td><code>string</code></td><td><p>subject content</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service.actions.emailList"></a>

#### actions.emailList ⇒ <code>array</code>

get all emails in a cache

**Kind**: static property of [<code>actions</code>](#module_email-service.actions)  
**Returns**: <code>array</code> - - email list  
**Actions**:  
<a name="module_email-service.actions.addEmail"></a>

#### actions.addEmail ⇒ <code>object</code>

add email to cache email list

**Kind**: static property of [<code>actions</code>](#module_email-service.actions)  
**Returns**: <code>object</code> - - if the email was processed return the status of processing  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>email</td><td><code>string</code> | <code>array</code></td><td><p>email or array of emails</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service.actions.delEmail"></a>

#### actions.delEmail ⇒ <code>object</code>

remove email to cache email list

**Kind**: static property of [<code>actions</code>](#module_email-service.actions)  
**Returns**: <code>object</code> - - if the email was processed return the status of processing  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>email</td><td><code>string</code> | <code>array</code></td><td><p>email or array of emails</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service.methods"></a>

### email-service.methods

Methods

**Kind**: static property of [<code>email-service</code>](#module_email-service)

- [.methods](#module_email-service.methods)
  - [.getContacts()](#module_email-service.methods.getContacts) ⇒ <code>array</code>
  - [.parseSaveCacheResponse(array, emails)](#module_email-service.methods.parseSaveCacheResponse) ⇒ <code>array</code>
  - [.manageContacts(type, emails)](#module_email-service.methods.manageContacts) ⇒ <code>array</code>

<a name="module_email-service.methods.getContacts"></a>

#### methods.getContacts() ⇒ <code>array</code>

get all emails in a cache

**Kind**: static method of [<code>methods</code>](#module_email-service.methods)  
**Returns**: <code>array</code> - - email list  
**Methods**:  
<a name="module_email-service.methods.parseSaveCacheResponse"></a>

#### methods.parseSaveCacheResponse(array, emails) ⇒ <code>array</code>

parse array return from redis

**Kind**: static method of [<code>methods</code>](#module_email-service.methods)  
**Returns**: <code>array</code> - - [{ email: email from emails array, status: true or false }]  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>array</td><td><code>array</code></td><td><p>array of arrays return from redis multi</p>
</td>
    </tr><tr>
    <td>emails</td><td><code>array</code></td><td><p>emails array</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service.methods.manageContacts"></a>

#### methods.manageContacts(type, emails) ⇒ <code>array</code>

add or remove emails to cache

**Kind**: static method of [<code>methods</code>](#module_email-service.methods)  
**Returns**: <code>array</code> - - [{ email: email from emails array, status: true or false }]  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>string</code></td><td><p>add or rem</p>
</td>
    </tr><tr>
    <td>emails</td><td><code>array</code></td><td><p>emails array</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_email-service.started"></a>

### email-service.started()

Service started lifecycle event handler
add default email to list cache

**Kind**: static method of [<code>email-service</code>](#module_email-service)  
**Started**:  
<a name="module_painelweb-service"></a>

## painelweb-service : <code>service</code>

upload files to a boleto integration

**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>dependencies</td><td><code>object</code></td><td><p>dependencies</p>
</td>
    </tr><tr>
    <td>mixins</td><td><code>object</code></td><td><p>mixins</p>
</td>
    </tr><tr>
    <td>actions</td><td><code>object</code></td><td><p>actions</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>created</td><td><code>function</code></td><td><p>created</p>
</td>
    </tr>  </tbody>
</table>

- [painelweb-service](#module_painelweb-service) : <code>service</code>
  - [.settings](#module_painelweb-service.settings)
  - [.mixins](#module_painelweb-service.mixins)
  - [.actions](#module_painelweb-service.actions)
    - [.upload](#module_painelweb-service.actions.upload) ⇒ <code>object</code>
  - [.methods](#module_painelweb-service.methods)
    - [.createJSON()](#module_painelweb-service.methods.createJSON) ⇒ <code>buffer</code>
    - [.dataParse(files, filePath)](#module_painelweb-service.methods.dataParse) ⇒ <code>object</code>

<a name="module_painelweb-service.settings"></a>

### painelweb-service.settings

Settings

**Kind**: static property of [<code>painelweb-service</code>](#module_painelweb-service)  
<a name="module_painelweb-service.mixins"></a>

### painelweb-service.mixins

Mixins

**Kind**: static property of [<code>painelweb-service</code>](#module_painelweb-service)  
**Mixes**: [<code>axios</code>](#axios)  
<a name="module_painelweb-service.actions"></a>

### painelweb-service.actions

Actions

**Kind**: static property of [<code>painelweb-service</code>](#module_painelweb-service)  
<a name="module_painelweb-service.actions.upload"></a>

#### actions.upload ⇒ <code>object</code>

upload file to painelWeb

**Kind**: static property of [<code>actions</code>](#module_painelweb-service.actions)  
**Returns**: <code>object</code> - - response from request  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>file</td><td><code>object</code></td><td><p>{buffer, filename}</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_painelweb-service.methods"></a>

### painelweb-service.methods

Methods

**Kind**: static property of [<code>painelweb-service</code>](#module_painelweb-service)

- [.methods](#module_painelweb-service.methods)
  - [.createJSON()](#module_painelweb-service.methods.createJSON) ⇒ <code>buffer</code>
  - [.dataParse(files, filePath)](#module_painelweb-service.methods.dataParse) ⇒ <code>object</code>

<a name="module_painelweb-service.methods.createJSON"></a>

#### methods.createJSON() ⇒ <code>buffer</code>

create a specific json configuration for integration with painelWeb

**Kind**: static method of [<code>methods</code>](#module_painelweb-service.methods)  
**Returns**: <code>buffer</code> - JSON buffer  
**Actions**:  
<a name="module_painelweb-service.methods.dataParse"></a>

#### methods.dataParse(files, filePath) ⇒ <code>object</code>

parse files into a form

**Kind**: static method of [<code>methods</code>](#module_painelweb-service.methods)  
**Returns**: <code>object</code> - - parsed form data  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>files</td><td><code>array</code></td><td><p>array of files</p>
</td>
    </tr><tr>
    <td>filePath</td><td><code>string</code></td><td><p>path of files</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_workflow-service"></a>

## workflow-service : <code>service</code>

listen queue and execute workflow get data, call service to save data on redis / csv, call service to upload file on integration and call service to send email confirmation;

**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>dependencies</td><td><code>object</code></td><td><p>dependencies</p>
</td>
    </tr><tr>
    <td>mixins</td><td><code>object</code></td><td><p>mixins</p>
</td>
    </tr><tr>
    <td>actions</td><td><code>object</code></td><td><p>actions</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>created</td><td><code>function</code></td><td><p>created</p>
</td>
    </tr>  </tbody>
</table>

- [workflow-service](#module_workflow-service) : <code>service</code>
  - [.settings](#module_workflow-service.settings)
  - [.dependencies](#module_workflow-service.dependencies)
  - [.mixins](#module_workflow-service.mixins)
  - [.actions](#module_workflow-service.actions)
    - [.data](#module_workflow-service.actions.data) ⇒ <code>String</code>
  - [.events](#module_workflow-service.events)
    - [.queue.CSV](#module_workflow-service.events.queue.CSV)
    - [.queue.succeeded](#module_workflow-service.events.queue.succeeded)
    - [.queue.failed](#module_workflow-service.events.queue.failed)
  - [.methods](#module_workflow-service.methods)
    - [.timer()](#module_workflow-service.methods.timer) ⇒ <code>object</code>
    - [.save(data)](#module_workflow-service.methods.save) ⇒ <code>object</code>
    - [.push()](#module_workflow-service.methods.push) ⇒ <code>string</code>
  - [.started()](#module_workflow-service.started)

<a name="module_workflow-service.settings"></a>

### workflow-service.settings

Settings

**Kind**: static property of [<code>workflow-service</code>](#module_workflow-service)  
<a name="module_workflow-service.dependencies"></a>

### workflow-service.dependencies

dependencies

**Kind**: static property of [<code>workflow-service</code>](#module_workflow-service)  
**Requires**: <code>module:painelWeb-service</code>, [<code>email-service</code>](#module_email-service), [<code>csv-service</code>](#module_csv-service)  
<a name="module_workflow-service.mixins"></a>

### workflow-service.mixins

Mixins

**Kind**: static property of [<code>workflow-service</code>](#module_workflow-service)  
**Mixes**: [<code>redis</code>](#redis), [<code>queue</code>](#queue)  
<a name="module_workflow-service.actions"></a>

### workflow-service.actions

Actions

**Kind**: static property of [<code>workflow-service</code>](#module_workflow-service)  
<a name="module_workflow-service.actions.data"></a>

#### actions.data ⇒ <code>String</code>

start workflow to save data

**Kind**: static property of [<code>actions</code>](#module_workflow-service.actions)  
**Returns**: <code>String</code> - - return success message  
**Actions**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>Object</code></td><td><p>data to save</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_workflow-service.events"></a>

### workflow-service.events

Events

**Kind**: static property of [<code>workflow-service</code>](#module_workflow-service)

- [.events](#module_workflow-service.events)
  - [.queue.CSV](#module_workflow-service.events.queue.CSV)
  - [.queue.succeeded](#module_workflow-service.events.queue.succeeded)
  - [.queue.failed](#module_workflow-service.events.queue.failed)

<a name="module_workflow-service.events.queue.CSV"></a>

#### events.queue.CSV

listem queue job

**Kind**: static property of [<code>events</code>](#module_workflow-service.events)  
**Emits**: <code>concat.event:CSV</code>

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>job</td><td><code>Object</code></td><td><p>bee-queue job object</p>
</td>
    </tr><tr>
    <td>done</td><td><code>function</code></td><td><p>done callback function</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_workflow-service.events.queue.succeeded"></a>

#### events.queue.succeeded

listem queue success event

**Kind**: static property of [<code>events</code>](#module_workflow-service.events)  
**Emits**: <code>concat.event:succeeded</code>

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>job</td><td><code>Object</code></td><td><p>bee-queue job object</p>
</td>
    </tr><tr>
    <td>result</td><td><code>Object</code></td><td><p>result Object</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_workflow-service.events.queue.failed"></a>

#### events.queue.failed

listem queue error event

**Kind**: static property of [<code>events</code>](#module_workflow-service.events)  
**Emits**: <code>concat.event:succeeded</code>

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>err</td><td><code>Object</code></td><td><p>bee-queue error object</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_workflow-service.methods"></a>

### workflow-service.methods

Methods

**Kind**: static property of [<code>workflow-service</code>](#module_workflow-service)

- [.methods](#module_workflow-service.methods)
  - [.timer()](#module_workflow-service.methods.timer) ⇒ <code>object</code>
  - [.save(data)](#module_workflow-service.methods.save) ⇒ <code>object</code>
  - [.push()](#module_workflow-service.methods.push) ⇒ <code>string</code>

<a name="module_workflow-service.methods.timer"></a>

#### methods.timer() ⇒ <code>object</code>

create a timer

**Kind**: static method of [<code>methods</code>](#module_workflow-service.methods)  
**Returns**: <code>object</code> - - timeout object  
**Methods**:  
<a name="module_workflow-service.methods.save"></a>

#### methods.save(data) ⇒ <code>object</code>

Call service to save data on cache

**Kind**: static method of [<code>methods</code>](#module_workflow-service.methods)  
**Returns**: <code>object</code> - - return from csv save service  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>object</code></td><td><p>data from queue worker</p>
</td>
    </tr>  </tbody>
</table>

<a name="module_workflow-service.methods.push"></a>

#### methods.push() ⇒ <code>string</code>

call painelWeb integration service and call service to send confirmation email

**Kind**: static method of [<code>methods</code>](#module_workflow-service.methods)  
**Returns**: <code>string</code> - - return success message  
**Methods**:  
<a name="module_workflow-service.started"></a>

### workflow-service.started()

start listen works

**Kind**: static method of [<code>workflow-service</code>](#module_workflow-service)  
**Started**:  
<a name="axios"></a>

## axios

Service mixin to send http request using axios with retry

**Kind**: global mixin  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>created</td><td><code>function</code></td><td><p>created</p>
</td>
    </tr>  </tbody>
</table>

**Example**

```js
const response = await this.$axios.request(Options);
```

- [axios](#axios)
  - [.settings](#axios.settings)
    - [.axios](#axios.settings.axios) : <code>Object</code>
      - [.retry](#axios.settings.axios.retry) : <code>Number</code>
      - [.delay](#axios.settings.axios.delay) : <code>Number</code>
      - [.options](#axios.settings.axios.options) : <code>Object</code>
  - [.methods](#axios.methods)
    - [.axios(retry, delay, options)](#axios.methods.axios) ⇒ <code>object</code> \| <code>function</code>
  - [.created()](#axios.created)

<a name="axios.settings"></a>

### axios.settings

Default Settings

**Kind**: static property of [<code>axios</code>](#axios)

- [.settings](#axios.settings)
  - [.axios](#axios.settings.axios) : <code>Object</code>
    - [.retry](#axios.settings.axios.retry) : <code>Number</code>
    - [.delay](#axios.settings.axios.delay) : <code>Number</code>
    - [.options](#axios.settings.axios.options) : <code>Object</code>

<a name="axios.settings.axios"></a>

#### settings.axios : <code>Object</code>

axios object configuration.

**Kind**: static property of [<code>settings</code>](#axios.settings)

- [.axios](#axios.settings.axios) : <code>Object</code>
  - [.retry](#axios.settings.axios.retry) : <code>Number</code>
  - [.delay](#axios.settings.axios.delay) : <code>Number</code>
  - [.options](#axios.settings.axios.options) : <code>Object</code>

<a name="axios.settings.axios.retry"></a>

##### axios.retry : <code>Number</code>

number of attempts

**Kind**: static property of [<code>axios</code>](#axios.settings.axios)  
<a name="axios.settings.axios.delay"></a>

##### axios.delay : <code>Number</code>

delay between attempts

**Kind**: static property of [<code>axios</code>](#axios.settings.axios)  
<a name="axios.settings.axios.options"></a>

##### axios.options : <code>Object</code>

axios default options [https://github.com/axios/axios](https://github.com/axios/axios)

**Kind**: static property of [<code>axios</code>](#axios.settings.axios)  
<a name="axios.methods"></a>

### axios.methods

methods

**Kind**: static property of [<code>axios</code>](#axios)  
<a name="axios.methods.axios"></a>

#### methods.axios(retry, delay, options) ⇒ <code>object</code> \| <code>function</code>

create a function to call http with axios

**Kind**: static method of [<code>methods</code>](#axios.methods)  
**Returns**: <code>object</code> \| <code>function</code> - Axios function  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>retry</td><td><code>number</code></td><td><p>number of attempts</p>
</td>
    </tr><tr>
    <td>delay</td><td><code>number</code></td><td><p>delay between attempts</p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>axios default options  <a href="https://github.com/axios/axios">https://github.com/axios/axios</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="axios.created"></a>

### axios.created()

Service created lifecycle event handler
define this.\$axios as axios Object

**Kind**: static method of [<code>axios</code>](#axios)  
**Created**:  
<a name="email"></a>

## email

Service mixin to create and send email

**Kind**: global mixin  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>started</td><td><code>function</code></td><td><p>started</p>
</td>
    </tr>  </tbody>
</table>

**Example**

```js
const message = this.messageBuild(to, from, subject, text, attachments);
const response = this.sendEmail(message);
```

- [email](#email)
  - [.settings](#email.settings)
    - [.email](#email.settings.email) : <code>Object</code>
      - [.key](#email.settings.email.key) : <code>String</code>
  - [.methods](#email.methods)
    - [.Message](#email.methods.Message)
      - [.template](#email.methods.Message.template) : <code>function</code>
      - [.addAttachment](#email.methods.Message.addAttachment) : <code>function</code>
      - [.addHTML](#email.methods.Message.addHTML) : <code>function</code>
      - [.get](#email.methods.Message.get) ⇒ <code>object</code>
    - [.messageBuild(to, from, subject, text, html, attachments)](#email.methods.messageBuild) ⇒ <code>message</code>
    - [.sendEmail(message)](#email.methods.sendEmail) ⇒ <code>object</code>
  - [.started()](#email.started)

<a name="email.settings"></a>

### email.settings

Default Settings

**Kind**: static property of [<code>email</code>](#email)

- [.settings](#email.settings)
  - [.email](#email.settings.email) : <code>Object</code>
    - [.key](#email.settings.email.key) : <code>String</code>

<a name="email.settings.email"></a>

#### settings.email : <code>Object</code>

email object configuration.

**Kind**: static property of [<code>settings</code>](#email.settings)  
<a name="email.settings.email.key"></a>

##### email.key : <code>String</code>

key to connect on sendgrid

**Kind**: static property of [<code>email</code>](#email.settings.email)  
<a name="email.methods"></a>

### email.methods

methods

**Kind**: static property of [<code>email</code>](#email)

- [.methods](#email.methods)
  - [.Message](#email.methods.Message)
    - [.template](#email.methods.Message.template) : <code>function</code>
    - [.addAttachment](#email.methods.Message.addAttachment) : <code>function</code>
    - [.addHTML](#email.methods.Message.addHTML) : <code>function</code>
    - [.get](#email.methods.Message.get) ⇒ <code>object</code>
  - [.messageBuild(to, from, subject, text, html, attachments)](#email.methods.messageBuild) ⇒ <code>message</code>
  - [.sendEmail(message)](#email.methods.sendEmail) ⇒ <code>object</code>

<a name="email.methods.Message"></a>

#### methods.Message

**Kind**: static class of [<code>methods</code>](#email.methods)

- [.Message](#email.methods.Message)
  - [.template](#email.methods.Message.template) : <code>function</code>
  - [.addAttachment](#email.methods.Message.addAttachment) : <code>function</code>
  - [.addHTML](#email.methods.Message.addHTML) : <code>function</code>
  - [.get](#email.methods.Message.get) ⇒ <code>object</code>

<a name="email.methods.Message.template"></a>

##### Message.template : <code>function</code>

**Kind**: static property of [<code>Message</code>](#email.methods.Message)

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>templateId</td><td><code>string</code></td><td><p>template ID from sendgrid</p>
</td>
    </tr><tr>
    <td>dynamicTemplateData</td><td><code>object</code></td><td><p>variables config in template on sendgrid</p>
</td>
    </tr>  </tbody>
</table>

<a name="email.methods.Message.addAttachment"></a>

##### Message.addAttachment : <code>function</code>

**Kind**: static property of [<code>Message</code>](#email.methods.Message)

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>attachments</td><td><code>object</code></td><td><p>{filePath: &#39;path/of/files&#39;, files: [array, of, filenames, in, a, path]}</p>
</td>
    </tr>  </tbody>
</table>

<a name="email.methods.Message.addHTML"></a>

##### Message.addHTML : <code>function</code>

**Kind**: static property of [<code>Message</code>](#email.methods.Message)

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>html</td><td><code>string</code></td><td><p>html in string</p>
</td>
    </tr>  </tbody>
</table>

<a name="email.methods.Message.get"></a>

##### Message.get ⇒ <code>object</code>

**Kind**: static property of [<code>Message</code>](#email.methods.Message)  
**Returns**: <code>object</code> - - return only attributes  
<a name="email.methods.messageBuild"></a>

#### methods.messageBuild(to, from, subject, text, html, attachments) ⇒ <code>message</code>

Create a message object to send email.

**Kind**: static method of [<code>methods</code>](#email.methods)  
**Returns**: <code>message</code> - - Message object  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>to</td><td><code><a href="#email">email</a></code> | <code><a href="#email">array.&lt;email&gt;</a></code></td><td><p>email or mailing list you want to send to</p>
</td>
    </tr><tr>
    <td>from</td><td><code><a href="#email">email</a></code></td><td><p>sender email</p>
</td>
    </tr><tr>
    <td>subject</td><td><code>string</code></td><td><p>subject of the email to be sent</p>
</td>
    </tr><tr>
    <td>text</td><td><code>string</code></td><td><p>body of email</p>
</td>
    </tr><tr>
    <td>html</td><td><code>string</code></td><td><p>body of email</p>
</td>
    </tr><tr>
    <td>attachments</td><td><code>object</code></td><td><p>{filePath: &#39;path/of/files&#39;, files: [array, of, filenames, in, a, path]}</p>
</td>
    </tr>  </tbody>
</table>

<a name="email.methods.sendEmail"></a>

#### methods.sendEmail(message) ⇒ <code>object</code>

Send email.

**Kind**: static method of [<code>methods</code>](#email.methods)  
**Returns**: <code>object</code> - - sendgrid response  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>message</td><td><code>object.&lt;message&gt;</code></td><td><p>message created with <a href="messageBuild">messageBuild</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="email.started"></a>

### email.started()

Service started lifecycle event handler
define this.\$sgMail with sendGride object

**Kind**: static method of [<code>email</code>](#email)  
**Started**:  
<a name="queue"></a>

## queue

Service mixin create a bee-queue worker

**Kind**: global mixin  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr>  </tbody>
</table>

**Example**

```js
const message = this.createJob(jobName, options);
```

- [queue](#queue)
  - [.methods](#queue.methods)
    - [.createJob(jobName, options)](#queue.methods.createJob) ⇒ <code>object</code>

<a name="queue.methods"></a>

### queue.methods

Methods

**Kind**: static property of [<code>queue</code>](#queue)  
<a name="queue.methods.createJob"></a>

#### methods.createJob(jobName, options) ⇒ <code>object</code>

Create a message object to send email.

**Kind**: static method of [<code>methods</code>](#queue.methods)  
**Returns**: <code>object</code> - - bee-queue job object  
**Methods**:

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>jobName</td><td><code>string</code></td><td><p>name of job listener</p>
</td>
    </tr><tr>
    <td>options</td><td><code>object</code></td><td><p>bee-queue options with redis client @see <a href="https://github.com/bee-queue/bee-queue">https://github.com/bee-queue/bee-queue</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="redis"></a>

## redis

Service mixin to access Redis entities

**Kind**: global mixin  
**Namespace\***:  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>settings</td><td><code>object</code></td><td><p>Settings</p>
</td>
    </tr><tr>
    <td>methods</td><td><code>object</code></td><td><p>methods</p>
</td>
    </tr><tr>
    <td>created</td><td><code>function</code></td><td><p>Service created lifecycle event handler</p>
</td>
    </tr><tr>
    <td>started</td><td><code>function</code></td><td><p>Service started lifecycle event handler</p>
</td>
    </tr><tr>
    <td>stopped</td><td><code>function</code></td><td><p>Service stopped lifecycle event handler</p>
</td>
    </tr>  </tbody>
</table>

- [redis](#redis)
  - [.settings](#redis.settings)
    - [.redis](#redis.settings.redis) : <code>Object</code>
  - [.methods](#redis.methods)
    - [.connect()](#redis.methods.connect) ⇒ <code>string</code>
    - [.checkConnection()](#redis.methods.checkConnection) ⇒ <code>string</code>
    - [.disconnect()](#redis.methods.disconnect) ⇒ <code>string</code>
  - [.created()](#redis.created)
  - [.started()](#redis.started)
  - [.stopped()](#redis.stopped)

<a name="redis.settings"></a>

### redis.settings

Default settings

**Kind**: static property of [<code>redis</code>](#redis)  
<a name="redis.settings.redis"></a>

#### settings.redis : <code>Object</code>

redis object configuration @see https://github.com/luin/ioredis

**Kind**: static property of [<code>settings</code>](#redis.settings)  
<a name="redis.methods"></a>

### redis.methods

Methods

**Kind**: static property of [<code>redis</code>](#redis)

- [.methods](#redis.methods)
  - [.connect()](#redis.methods.connect) ⇒ <code>string</code>
  - [.checkConnection()](#redis.methods.checkConnection) ⇒ <code>string</code>
  - [.disconnect()](#redis.methods.disconnect) ⇒ <code>string</code>

<a name="redis.methods.connect"></a>

#### methods.connect() ⇒ <code>string</code>

connect and subscribe events from redis.

**Kind**: static method of [<code>methods</code>](#redis.methods)  
**Returns**: <code>string</code> - - redis connection status confirmation  
**Methods**:  
<a name="redis.methods.checkConnection"></a>

#### methods.checkConnection() ⇒ <code>string</code>

check connection and reconnect with necessary

**Kind**: static method of [<code>methods</code>](#redis.methods)  
**Returns**: <code>string</code> - - connection status  
**Methods**:  
<a name="redis.methods.disconnect"></a>

#### methods.disconnect() ⇒ <code>string</code>

disconnect redis server

**Kind**: static method of [<code>methods</code>](#redis.methods)  
**Returns**: <code>string</code> - - connection status  
**Methods**:  
<a name="redis.created"></a>

### redis.created()

define this.\$redis = new Redis client
and connect

**Kind**: static method of [<code>redis</code>](#redis)  
**Created**:  
<a name="redis.started"></a>

### redis.started()

check Connection

**Kind**: static method of [<code>redis</code>](#redis)  
**Started**:  
<a name="redis.stopped"></a>

### redis.stopped()

disconnect to redis service

**Kind**: static method of [<code>redis</code>](#redis)  
**Stopped**:
